package com.wzl.app.weixin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wzl.app.common.utils.PageUtils;
import com.wzl.app.weixin.entity.UserEntity;

import java.util.Map;

/**
 * 
 *
 * @author weizhilong
 * @email sunlightcs@gmail.com
 * @date 2023-01-22 15:58:42
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean saveUser(UserEntity user);
}

