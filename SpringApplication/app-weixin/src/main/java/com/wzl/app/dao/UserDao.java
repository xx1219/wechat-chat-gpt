package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-01-22 15:33:56
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
