package com.wzl.wxdemo.service;

import com.wzl.wxdemo.common.R;

import java.util.Map;

public interface WXService {

    R login(String json) throws Exception;

    Map<String,String> getAllInfo(String code) throws Exception;

}
