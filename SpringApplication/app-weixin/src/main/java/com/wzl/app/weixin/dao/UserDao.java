package com.wzl.app.weixin.dao;

import com.wzl.app.weixin.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author weizhilong
 * @email sunlightcs@gmail.com
 * @date 2023-01-22 15:58:42
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
