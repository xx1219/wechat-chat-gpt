package com.wzl.app.weixin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppWeixinApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppWeixinApplication.class, args);
    }

}
