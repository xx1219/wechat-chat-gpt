package com.wzl.app.weixin.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzl.app.common.utils.PageUtils;
import com.wzl.app.common.utils.Query;

import com.wzl.app.weixin.dao.UserDao;
import com.wzl.app.weixin.entity.UserEntity;
import com.wzl.app.weixin.service.UserService;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                new QueryWrapper<UserEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public boolean saveUser(UserEntity user) {
        long repeatUsername = count(new QueryWrapper<UserEntity>().eq("username" , user.getUsername()));
        if (repeatUsername >= 1) {
            return false;
        }
        long nickname = count(new QueryWrapper<UserEntity>().eq("nickname", user.getNickname()));
        save(user);
        return true;
    }

}