package com.wzl.wxdemo.service.impl;

import com.alibaba.fastjson.JSON;
import com.wzl.wxdemo.common.R;
import com.wzl.wxdemo.service.WXService;
import com.wzl.wxdemo.utils.HttpUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WXServiceImpl implements WXService {

    @Value("${weixin.AppSecret}")
    private String secret;

    @Value("${weixin.AppID}")
    private String appId;

    @Value("${weixin.baseURL}")
    private String baseUrl;

    @Override
    public R login(String code) throws Exception {
        String openId = getOpenId(code);
        if ("".equals(openId)) {
            return R.fail();
        }
        // 返回数据
        return R.ok().put("data" , openId);
    }

    @Override
    public Map<String,String> getAllInfo(String code) throws Exception {
        return getAll(code);
    }

    /**
     * 登录凭证校验, 返回 openid， session_key， unionid 等
     * 详见 https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
     * @param code 登录时获取的 code
     * @return 请求返回数据
     * @throws Exception 请求异常
     */
    private String authCode2Session(String code) throws Exception {
        HashMap<String, String> data = new HashMap<>(5);
        data.put("appid", appId);
        data.put("secret", secret);
        data.put("js_code", code);
        data.put("grant_type", "authorization_code");
        return HttpUtil.setHttpRequest(baseUrl + "/sns/jscode2session", data, "GET");
    }


    private Map<String , String> getAll(String code) throws Exception {
        HashMap<String , String> map = JSON.parseObject(authCode2Session(code), HashMap.class);
        return map;
    }

    /**
     * 请求微信服务器， 获取 openid
     * @param code 登录时获取的 code
     * @return openid
     * @throws Exception 异常
     */
    private String getOpenId(String code) throws Exception {
        // 请求微信服务器，获取用户 oppenid
        HashMap map;
        map = JSON.parseObject(authCode2Session(code), HashMap.class);
        String openid = map.get("openid").toString();
        // 请求失败处理
        if ("".equals(openid) || openid == null) {
            return "";
        }

        // 请求成功，并且返回数据
        return map.get("openid").toString();
    }
}
