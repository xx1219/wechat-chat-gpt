import * as koa from 'koa'
import * as koaRouter from 'koa-router'
import * as koaBody from 'koa-body'

import core from '../core/chatgpt/core';

const app = new koa();
app.use(koaBody.koaBody({multipart: true}));

const router = new koaRouter();

router.post('/api/chat', async (ctx) => {
    const content: string = ctx.request.body.content;
    ctx.body = await core.getResult(content);
})

app.use(router.routes());

app.use(async (ctx, next) => {
    ctx.body = 'Hello   world'
})

app.listen(3000 , () => {
    console.log('koa is started localhost:3000')
})