import { Configuration, OpenAIApi } from 'openai'
import chat from '../config/chat';
import commonConfig from '../config/common';

class Core {
  private configuration: Configuration;
  private openai: OpenAIApi;
  private config: commonConfig;
  constructor() {
    this.configuration = new Configuration({
      apiKey: process.env.OPENAI_KEY,
    });
    this.openai = new OpenAIApi(this.configuration);
    this.config = chat;
  }
  public async response(prompt: string) {
    let rep = await this.openai.createCompletion({
      model: this.config.model,
      prompt,
      temperature: this.config.temperature,
      max_tokens: this.config.max_tokens,
      top_p: this.config.top_p,
      frequency_penalty: this.config.frequency_penalty,
      presence_penalty: this.config.presence_penalty,
      stop: this.config.stop,
    })
    return rep;
  }
  public async getResult(prompt: string) {
    return await (await this.response(prompt)).data.choices;
  }

}

export default new Core();