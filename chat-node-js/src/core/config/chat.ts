/**
 * Open ended conversation with an AI assistant.
Prompt
The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.

Human: Hello, who are you?
AI: I am an AI created by OpenAI. How can I help you today?
Human: I'd like to cancel my subscription.
AI:
Sample response
I understand, I can help you with canceling your subscription. Please provide me with your account details so that I can begin processing the cancellation.
 * 
 * 
 * model: "text-davinci-003",
  prompt: "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman: Hello, who are you?\nAI: I am an AI created by OpenAI. How can I help you today?\nHuman: I'd like to cancel my subscription.\nAI:",
  temperature: 0.9,
  max_tokens: 150,
  top_p: 1,
  frequency_penalty: 0.0,
  presence_penalty: 0.6,
  stop: [" Human:", " AI:"],
 */

import commonConfig from "./common";



class Chat implements commonConfig {
    model: string;
    temperature: number;
    max_tokens: number;
    top_p: number;
    frequency_penalty: number;
    presence_penalty: number;
    stop: string[];
    
    constructor() {
        this.model="text-davinci-003"
        this.temperature=0.9
        this.max_tokens=2000
        this.top_p=1
        this.frequency_penalty=0.0
        this.presence_penalty=0.6
        this.stop=[" Human:", " AI:"]
    }
}

export default new Chat();